from time import sleep, localtime
# from PyQt5.QtGui import QImage, QPixmap
# from PyQt5.QtWidgets import QDialog, QLabel
from cv2 import *
import numpy as np
import pyzbar.pyzbar as pyzbar
from threading import Thread
from PIL import Image
import numpy as np
import sys
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QLabel


class QrVideoThread(Thread):

    font = cv2.FONT_HERSHEY_COMPLEX_SMALL
    FRAME_DELAY = 0.01

    def __init__(self, information_list):
        Thread.__init__(self)

        self.cap = cv2.VideoCapture(0)

        self.showVideoPreview = True
        self.interrupted = False
        self.information_list = information_list
        self.data_buffer = []
        # self.video = App()
        self.hsv_min = np.array((20, 0, 0), np.uint8)
        self.hsv_max = np.array((100, 255, 255), np.uint8)

    def run(self):
        # window = QDialog()
        # window.setWindowTitle("QR code scan video preview")
        # video_frame = QLabel(window)

        while not self.interrupted:
            _, videodisk = self.cap.read()

            # window.close()

            decode_data = pyzbar.decode(videodisk)

            rectangular_data = cv2.cvtColor(videodisk, cv2.COLOR_BGR2HSV)
            # tresh = cv2.inRange(rectangular_data, self.hsv_min, self.hsv_max)
            # contouirs, hierarchy = cv2.findContours(tresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            # for cont in contouirs:
            #     if len(cont) > 1500:
            #         rect = cv2.minAreaRect(cont)
            #         points = cv2.boxPoints(rect)
            #         points = np.int0(points)
            #         x_center = points[0][0] + points[2][0]
            #         y_center = points[0][1] + points[2][1]
            #         cv2.drawContours(videodisk, [points], 0,(255, 0, 0), 2)


            if decode_data:
                if str(decode_data[0].data) not in self.data_buffer:
                    self.information_list.append(see_time()
                                                 + "<font color = blue> Decode new item: </font>"
                                                 + str((decode_data[0].data).decode('utf-8')).replace('b', ''))
                    self.data_buffer.append(str(decode_data[0].data))

            # if self.showVideoPreview:
            #     for decode in decode_data:
            #         cv2.putText(videodisk, decode.data.decode("utf-8").replace('b', '')
            #                     , (50, 50), self.font, 3, (0, 255, 0), 1)
            #
            #     img = Image.fromarray(videodisk, 'RGB')
                # img.show()
                # sleep(1)
                # self.video.initUI()
                # cv2.imshow("QR code scan", videodisk)
                # cv2.namedWindow('bam', 0)

            key = cv2.waitKey(10)
            # img.close()
            if key == 27:
                return 0
                #
                # img = QImage(videodisk, videodisk.shape[1], videodisk.shape[0], QImage.Format_RGB888)
                # pix = QPixmap.fromImage(img)
                # video_frame.setPixmap(pix)
                # window.show()

            # sleep(self.FRAME_DELAY)
        destroyAllWindows()

        # When everything done, release the capture



def see_time():
    return str(localtime().tm_hour) + ":" + str(localtime().tm_min) + ":" + str(localtime().tm_sec)

#
# class App(QMainWindow):
#     def __init__(self):
#         super().__init__()
#         self.title = 'Video'
#         self.left = 200
#         self.top = 200
#         self.width = 300
#         self.height = 300
#         self.initUI()
#
#     def initUI(self):
#         self.setWindowTitle(self.title)
#         self.setGeometry(self.left, self.top, self.width, self.height)
#
#         pixmap = QPixmap('image.png')
#
#         self.label = QLabel(self)
#         self.label.setPixmap(pixmap)
#         self.label.resize(pixmap.width(), pixmap.height())
#
#         self.resize(pixmap.width(), pixmap.height())
#
#
#     def show(self):
#         self.show()