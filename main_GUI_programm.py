import sys

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from time import localtime, sleep
import QrVideoThread
from NTI_GUI import UiMainWindow
from QrVideoThread import QrVideoThread
from SerialPortThread import SerialPortThread
from widgets.ExtendedQComboBox import ExtendedQComboBox


class MyWindow(QtWidgets.QMainWindow, QtWidgets.QApplication):
    def __init__(self):
        self.y_rail_position = [25, 134, 244, 350]
        self.qrSerialPortThread = None
        super(MyWindow, self).__init__()
        self.ui = UiMainWindow()
        self.ui.setupUi(self)
        self.qrVideoThread = None
        self.max_x_pos = 990
        self.max_y_pos = 390
        self.servo = [60, 95]

        # connection title
        self.ui.setBaudrateBox.addItems(
            ["2400", "4800", "9600", "14400", "19200", "28800", "38400", "57600", "76800", "115200",
             "230400", "250000", "500000"])
        self.ui.setBaudrateBox.setCurrentIndex(9)
        self.ui.setSerialBox = ExtendedQComboBox(self.ui.setSerialBox)
        # self.ui.setSerialBox.setObjectName("setSerialBox")
        # self.ui.setSerialBox.fill_values()
        self.ui.conectCNCButton.clicked.connect(self.connect_cnc_bridge)
        self.ui.cameraTranslation.stateChanged.connect(self.change_video_illustration)
        self.ui.cameraConect.setCheckable(True)
        self.ui.cameraConect.clicked[bool].connect(self.video_initializer)

        # first list(G-code console)
        self.ui.sendPushButton.pressed.connect(self.send_to_terminal)
        self.ui.terminalLine.editingFinished.connect(self.send_to_terminal)
        self.ui.gCodeTerminalList.setReadOnly(True)
        self.ui.gCodeTerminalList.textChanged.connect(self.Scroll)
        # self.ui.gCodeTerminalList.undoAvailable.connect(self.Scroll)

        # second list (History list)
        self.ui.informationList.setReadOnly(True)

        # third list

        # set grbl options
        # first tab widget(Travel)
        self.set_warning(self.ui.X_max_travel_line_edit, "Connect to GRBL!", True)
        # self.ui.X_max_travel_line_edit.textChanged.connect()
        self.set_warning(self.ui.Y_max_travel_line_edit, "Connect to GRBL!", True)
        self.set_warning(self.ui.Z_max_travel_line_edit, "Connect to GRBL!", True)

        # second tab widget(Rate)
        self.set_warning(self.ui.X_max_rate_line_edit, "Connect to GRBL!", True)
        self.set_warning(self.ui.Y_max_rate_line_edit, "Connect to GRBL!", True)
        self.set_warning(self.ui.Z_max_rate_line_edit, "Connect to GRBL!", True)

        # third tab widget(Acceleration)
        self.set_warning(self.ui.X_acceleration_line_edit, "Connect to GRBL!", True)
        self.set_warning(self.ui.Y_acceleration_line_edit, "Connect to GRBL!", True)
        self.set_warning(self.ui.Z_acceleration_line_edit, "Connect to GRBL!", True)

        # fourth tab widget(Step)
        self.set_warning(self.ui.X_steps_line_edit, "Connect to GRBL!", True)
        self.set_warning(self.ui.Y_steps_line_edit, "Connect to GRBL!", True)
        self.set_warning(self.ui.Z_steps_line_edit, "Connect to GRBL!", True)

        # new block areas options
        # self.ui.new_blocked_area_length_line_edit.textChanged.connect()

    #     four list
        self.ui.find_first.clicked.connect(self.find_first)

    def connect_cnc_bridge(self):
        if self.qrSerialPortThread is None:
            try:
                self.qrSerialPortThread = SerialPortThread(self.ui.setSerialBox.currentText(),
                                                           int(self.ui.setBaudrateBox.currentText()),
                                                           self.ui.gCodeTerminalList,
                                                           self.ui.conectCNCButton,
                                                           self.ui.informationList)
                # self.qrSerialPortThread.settings_complete.connect(self.set_cnc_options())
                # self.qrSerialPortThread.take_grbl_settings()
                # self.set_cnc_options()
                self.qrSerialPortThread.start()
                self.qrSerialPortThread.send_to_serial("G1 F5000")
                # self.qrSerialPortThread.grbl_settings_complite.connect(self.set_cnc_options())
                self.ui.conectCNCButton.setStyleSheet("background-color: rgb(43, 255, 28);")
                self.ui.conectCNCButton.setText("Done connection")
                self.ui.informationList.append(see_time() + "<font color = green> Connect to CNC Bridge </font>")
                # sleep(10)
                # self.set_cnc_options()
                # self.mount = serial.Serial("/dev/ttyACM0", "115200", timeout=0.2)
            except ValueError:
                # print("Error connection")
                self.qrSerialPortThread = None
                self.ui.conectCNCButton.setStyleSheet("background-color: rgb(255, 43, 28);")
                self.ui.conectCNCButton.setText("Error connection")
        else:
            self.qrSerialPortThread.finish()
            self.qrSerialPortThread = None
            self.ui.conectCNCButton.setStyleSheet("background-color: rgb(220, 220, 220);")
            self.ui.conectCNCButton.setText("Connect")
            self.ui.informationList.append(see_time() + "<font color = red> Disconnect to CNC Bridge </font>")

    def change_video_illustration(self, state):
        if self.qrVideoThread is not None:
            if state == Qt.Checked:
                self.qrVideoThread.showVideoPreview = True
            else:
                self.qrVideoThread.showVideoPreview = False

    def video_initializer(self, press):
        if press:
            self.qrVideoThread = QrVideoThread(self.ui.informationList)
            self.qrVideoThread.start()
            self.ui.cameraConect.setText("Connection done")
            self.ui.cameraConect.setStyleSheet("background-color: rgb(43, 255, 28);")
            self.ui.cameraTranslation.activateWindow()
            self.ui.informationList.append(see_time() + "<font color = green> Camera connect </font>")
        else:
            self.qrVideoThread.interrupted = True
            self.qrVideoThread = None
            self.ui.cameraConect.setText("Camera is not connect")
            self.ui.cameraConect.setStyleSheet("background-color: rgb(220, 220, 220);")
            self.ui.informationList.append(see_time() + "<font color = red> Camera disconnect </font>")

    def send_to_terminal(self):
        if self.qrSerialPortThread is not None:
            terminal_information = self.ui.terminalLine.text()
            self.ui.terminalLine.clear()
            self.ui.gCodeTerminalList.append(">>> " + str(terminal_information))

            if terminal_information == "clean":
                self.ui.gCodeTerminalList.setText("")
            elif "S" in terminal_information:
                if int(terminal_information[1:]) == 1:
                    self.mount.write((str(self.servo[1])).encode("utf-8"))
                else:
                    self.mount.write((str(self.servo[0])).encode("utf-8"))
            elif terminal_information != "" and "G28" != terminal_information\
                    and "t" not in terminal_information\
                    and "scan" != terminal_information:
                self.qrSerialPortThread.send_to_serial(terminal_information)

            if "G28" == terminal_information:
                self.mount.write((str(self.servo[1])).encode("utf-8"))
                self.qrSerialPortThread.send_to_serial("G28 Z")
                self.qrSerialPortThread.send_to_serial("G28 X Y")

            if "t" in terminal_information:
                if terminal_information[1:] == "1":
                    self.take()
                else:
                    self.paste()
            if "scan" in terminal_information:
                self.find_first()

        else:
            # self.ui.gCodeTerminalList.setTextColor(QColor.red())
            self.ui.gCodeTerminalList.append("<font color = red>Error: </font>"
                                             " <font color = black>check serial connection</font>")
            # self.ui.gCodeTerminalList.setTextColor(c = QColor.black())

    def Scroll(self):
        sb = self.ui.gCodeTerminalList.verticalScrollBar()
        sb.setValue(sb.maximum())
        # self.ui.gCodeTerminalList.scrollContentsBy(0, QAbstractSlider.maximum())

    def closeEvent(self, event):
        if self.qrSerialPortThread is not None:
            self.qrSerialPortThread.finish()
            if self.qrSerialPortThread.is_alive():
                self.qrSerialPortThread.join()

        if self.qrVideoThread is not None:
            self.qrVideoThread.interrupted = True
            if self.qrVideoThread.is_alive():
                self.qrVideoThread.join()
        super().closeEvent(event)

    @staticmethod
    def set_warning(widget, text, on_off):
        if on_off:
            widget.setStyleSheet("background-color: rgb(255, 200, 200);")
            widget.setReadOnly(True)
            widget.setText(text)
        else:
            widget.setStyleSheet("background-color: rgb(255, 255, 255);")
            widget.setReadOnly(False)
            widget.setText(text)

    def set_cnc_options(self):
        if self.qrSerialPortThread.g_code_settings:
            self.set_warning(self.ui.X_max_travel_line_edit, str(self.qrSerialPortThread.g_code_settings['$130']), False)
            self.set_warning(self.ui.Y_max_travel_line_edit, str(self.qrSerialPortThread.g_code_settings['$131']), False)
            self.set_warning(self.ui.Z_max_travel_line_edit, str(self.qrSerialPortThread.g_code_settings['$132']), False)

            self.set_warning(self.ui.X_max_rate_line_edit, str(self.qrSerialPortThread.g_code_settings['$110']), False)
            self.set_warning(self.ui.Y_max_rate_line_edit, str(self.qrSerialPortThread.g_code_settings['$111']), False)
            self.set_warning(self.ui.Z_max_rate_line_edit, str(self.qrSerialPortThread.g_code_settings['$112']), False)

            self.set_warning(self.ui.X_acceleration_line_edit, str(self.qrSerialPortThread.g_code_settings['$120']), False)
            self.set_warning(self.ui.Y_acceleration_line_edit, str(self.qrSerialPortThread.g_code_settings['$121']), False)
            self.set_warning(self.ui.Z_acceleration_line_edit, str(self.qrSerialPortThread.g_code_settings['$122']), False)

            self.set_warning(self.ui.X_steps_line_edit, str(self.qrSerialPortThread.g_code_settings['$100']), False)
            self.set_warning(self.ui.Y_steps_line_edit, str(self.qrSerialPortThread.g_code_settings['$101']), False)
            self.set_warning(self.ui.Z_steps_line_edit, str(self.qrSerialPortThread.g_code_settings['$102']), False)

    def find_first(self):
        if self.qrSerialPortThread and self.qrVideoThread:

            self.mount.write((str(self.servo[0])).encode("utf-8"))
            sleep(0.1)
            self.qrSerialPortThread.send_to_serial("G28 Z")
            self.qrSerialPortThread.send_to_serial("G28 X Y")
            self.ui.gCodeTerminalList.append("G28")
            sleep(10)
            self.qrSerialPortThread.send_to_serial("G90")
            self.ui.gCodeTerminalList.append("G90")
            self.qrSerialPortThread.send_to_serial("G1 F1000")
            self.ui.gCodeTerminalList.append("G1 F1000")
            self.qrVideoThread.data_buffer = []
            i = True
            for y_position in self.y_rail_position:
                if self.qrVideoThread.data_buffer:
                    break
                self.ui.gCodeTerminalList.append("G1 Y{y}".format(y = y_position))
                self.qrSerialPortThread.send_to_serial("G1 Y{y}".format(y = y_position))
                sleep(2)
                self.mount.write((str(self.servo[0])).encode("utf-8"))
                b = y_position
                if self.qrVideoThread.data_buffer:
                    break
                if i:
                    for x_position in range(0, self.max_x_pos, 2):
                        self.qrSerialPortThread.send_to_serial("G1 X{x}".format(x = x_position))
                        sleep(0.4)
                        self.ui.gCodeTerminalList.append("G1 X{x}".format(x = x_position))
                        a = x_position
                        if self.qrVideoThread.data_buffer:
                            break
                    i = False

                else:
                    for x_position in range(self.max_x_pos, 0, -2):
                        self.qrSerialPortThread.send_to_serial("G1 X{x}".format(x = x_position))
                        sleep(0.4)
                        self.ui.gCodeTerminalList.append("G1 X{x}".format(x = x_position))
                        a = x_position
                        if self.qrVideoThread.data_buffer:
                            break
                    i = True
                    if self.qrVideoThread.data_buffer:
                        break


            self.qrSerialPortThread.send_to_serial("G1 X{x} Y{y}".format(y = b, x = a + 160))
            self.qrSerialPortThread.send_to_serial("G1 F5000")
            self.ui.gCodeTerminalList.append("G1 F5000")
            sleep(10)
            self.take()
            # self.qrVideoThread.data_buffer = []
    def repeat(self):
        self.qrSerialPortThread.send_to_serial("G28 Z")
        sleep(20)
        for i in range(0, 11):
            self.mount.write((str(self.servo[0])).encode("utf-8"))
            sleep(0.1)
            self.qrSerialPortThread.send_to_serial("G1 Z30")
            sleep(3)
            self.mount.write((str(self.servo[1])).encode("utf-8"))
            sleep(0.1)
            self.qrSerialPortThread.send_to_serial("G1 Z0")
            sleep(3)
            self.qrSerialPortThread.send_to_serial("G1 Z30")
            sleep(3)
            self.mount.write((str(self.servo[0])).encode("utf-8"))
            sleep(0.1)
            self.qrSerialPortThread.send_to_serial("G1 Z0")
            sleep(3)

            self.mount.write((str(self.servo[1])).encode("utf-8"))
            sleep(0.1)

    def take(self):
        self.mount.write((str(self.servo[0])).encode("utf-8"))
        sleep(0.5)
        self.qrSerialPortThread.send_to_serial("G1 Z60")
        sleep(0.5)
        self.mount.write((str(self.servo[1])).encode("utf-8"))
        sleep(0.8)
        self.qrSerialPortThread.send_to_serial("G1 Z0")
        sleep(0.5)

    def paste(self):
        self.mount.write((str(self.servo[1])).encode("utf-8"))
        sleep(0.5)
        self.qrSerialPortThread.send_to_serial("G1 Z60")
        sleep(0.5)
        self.mount.write((str(self.servo[0])).encode("utf-8"))
        sleep(0.8)
        self.qrSerialPortThread.send_to_serial("G1 Z0")
        sleep(0.5)

def see_time():
    return str(localtime().tm_hour) + ":" + str(localtime().tm_min) + ":" + str(localtime().tm_sec)


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    application = MyWindow()
    application.show()
    sys.exit(app.exec_())
