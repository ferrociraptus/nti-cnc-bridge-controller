import serial.tools.list_ports
from PyQt5 import QtWidgets


class ExtendedQComboBox(QtWidgets.QComboBox):
    def __init__(self, *args, **kwargs):
        super(QtWidgets.QComboBox, self).__init__(*args, **kwargs)
        self.data = []
        self.fill_values()

    def showPopup(self):
        self.fill_values()

        super().showPopup()

    def fill_values(self):
        self.clear()
        self.addItem("")
        self.data.append(d.device for d in self.list_serial_devices())
        for i in self.data:
            self.addItems(i)
        self.setCurrentIndex(1)

    @staticmethod
    def list_serial_devices():
        return serial.tools.list_ports.comports()
