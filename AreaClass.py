
class RectangularArea:
    def __init__(self, length, waight, midpoint_position, name):
        self.length = length
        self.waight = waight
        self.midpoint_position = midpoint_position
        self.name = name
        self.upper_right_point = Point(self.midpoint_position.x + self.length / 2, self.midpoint_position.x + self.waight / 2)
        self.lower_right_point = Point(self.midpoint_position.x - self.length / 2, self.midpoint_position.x + self.waight / 2)
        self.upper_left_point = Point(self.midpoint_position.x + self.length / 2, self.midpoint_position.x - self.waight / 2)
        self.lower_left_point = Point(self.midpoint_position.x - self.length / 2, self.midpoint_position.x - self.waight / 2)
        self.points = [self.upper_right_point, self.lower_right_point, self.upper_left_point, self.lower_left_point]
        self.area = self.traingular_area(self.upper_right_point, self.lower_right_point, self.upper_left_point) + \
                    self.traingular_area(self.upper_right_point, self.lower_right_point, self.lower_left_point)


    def is_point_in_area(self, point):
        summ_all = self.traingular_area(self.upper_right_point, self.lower_right_point, point)\
                   + self.traingular_area(self.upper_left_point, self.lower_left_point, point)\
                   + self.traingular_area(self.upper_right_point, self.lower_left_point, point)\
                   + self.traingular_area(self.upper_left_point, self.lower_right_point, point)

        if summ_all > self.area:
            return False
        else:
            return True


    def traingular_area(self, point_1, point_2, point_3):
        ab = point_1.len(point_2)
        ac = point_1.len(point_3)
        bc = point_3.len(point_2)
        p = (ab + ac + bc) / 2
        return (p*(p-ab)*(p - ac)*(p - bc))**0.5



class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __radd__(self, other):
        self.x = self.x + other
        self.y = self.y + other

    def __rsub__(self, other):
        self.x = self.x - other
        self.y = self.y - other

    def len(self, other):
        return (((self.x - other.x)**2) + ((self.y - other.y)**2))**0.5

    def x_add(self, other):
        self.x = self.x + other

    def y_add(self, other):
        self.y = self.y + other

    def x_sub(self, other):
        self.x = self.x - other

    def y_sub(self, other):
        self.y = self.y - other
