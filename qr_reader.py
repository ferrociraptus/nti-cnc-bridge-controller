from time import sleep

from cv2 import *
import pyzbar.pyzbar as pyzbar


cap = cv2.VideoCapture(0)
font = cv2.FONT_HERSHEY_COMPLEX_SMALL

showVideoPreview = True
interrupted = False
while not interrupted:
    _, data = cap.read()

    decodeData = pyzbar.decode(data)
    for decode in decodeData:
        cv2.putText(data, str(decode.data), (50, 50), font, 3, (0, 255, 0), 1)

    if showVideoPreview:
        cv2.imshow("QR code scan", data)
        # showVideoPreview = False


    sleep(.05)

    key = cv2.waitKey(10)

    if key == 27:
        exit(0)



