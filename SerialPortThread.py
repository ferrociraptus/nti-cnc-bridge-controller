from threading import Thread
import serial.tools.list_ports
import serial
from time import sleep


# from PyQt5.QtCore import pyqtSignal, QObject


class SerialPortThread(Thread):
    # settings_complete = pyqtSignal()

    def __init__(self, port, baudrate, terminal_list, serial_button, history_list):
        Thread.__init__(self)
        # QObject.__init__(self)
        self.port = port
        self.terminal_list = terminal_list
        self.g_code_settings = {}
        self.history_list = history_list
        self.serial_button = serial_button
        self.run_available = True
        self.baudrate = baudrate
        self.serial = None
        self.connect()

    def connect(self):
        try:
            self.serial = serial.Serial(self.port, self.baudrate, timeout=0.2)
        except serial.SerialException as err:
            raise ValueError("Error open serial port [{}]: {}".format(self.port, err))

        data = ''
        while self.serial.in_waiting or data == '':
            sleep(.1)
            data = str(self.serial.readline().decode('utf-8')).replace("\r\n", '')
            self.terminal_list.append(data)

    # def run(self, terminal_list, serial_button):
    @property
    def run(self):
        if not self.serial.is_open:
            self.serial.open()

        # try:
        #     while self.serial.in_waiting:
        #         data = self.serial.readline()
        #         self.terminal_list.append(str(data.decode('utf-8')).replace("\r\n", ''))
        #
        #
        # except:
        #     self.history_list.append("<font color = red> Error: </font>"
        #                              " <font color = black>wrong serial connection  </font>")
        #     self.terminal_list.append("<font color = red> Error: wrong serial connection </font>")
        #     self.serial_button.setStyleSheet("background-color: rgb(255, 43, 28);")
        #     self.serial_button.setText("Error connection")
        #     return 0
        # self.take_grbl_settings()

        self.run_available = True
        while self.run_available:
            # sleep(0.1)
            while self.serial.in_waiting:
                try:
                    data = self.serial.readline()
                except:
                    self.terminal_list.append("<font color = red> Error: wrong serial connection </font>")
                    self.serial_button.setStyleSheet("background-color: rgb(255, 43, 28);")
                    self.serial_button.setText("Error connection")
                    return 0

                if data != "":
                    self.terminal_list.append(data.decode("utf-8").replace("\r\n", ''))
        return None

    def finish(self):
        self.serial.close()
        self.run_available = False

    def send_to_serial(self, information):
        if not self.serial.is_open:
            self.serial.open()
        self.serial.write((information + "\n").encode("utf-8"))

    def send_g_code(self, g_code):
        self.send_to_serial(g_code)
        self.serial.reset_input_buffer()

    def take_grbl_settings(self):
        self.send_to_serial('$$')
        # sleep(10)
        while self.serial.in_waiting or self.g_code_settings == {}:
            sleep(0.01)
            data = (self.serial.readline().decode("utf-8").replace('\r\n', ''))
            if data != "ok" and data:
                try:
                    data = data.split("=")
                    data = [data[0], (data[1].split(" "))[0]]
                except Exception as err:
                    print("Error parse serial port incoming data. Data: [{}]. Exception [{}]".format(data, err))
                if int(data[0][1:]) >= 100:
                    self.g_code_settings[data[0]] = float(data[1])

        print(self.g_code_settings)
